export default interface DataPreProcessor {
    readonly configObjectId: string;
    readonly name: string;
    readonly executionType: string;
    readonly jsCode: string;
    readonly order: number;
}
//# sourceMappingURL=data.preprocessor.model.d.ts.map