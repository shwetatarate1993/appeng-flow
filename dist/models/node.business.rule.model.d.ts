export default interface NodeBusinessRule {
    readonly configObjectId: string;
    readonly name: string;
    readonly executionType: string;
    readonly rule: string;
    readonly order: number;
}
//# sourceMappingURL=node.business.rule.model.d.ts.map