"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const appeng_db_1 = require("appeng-db");
const noflo_1 = __importDefault(require("noflo"));
const path = __importStar(require("path"));
const sinon_1 = __importDefault(require("sinon"));
const util_1 = __importDefault(require("util"));
const config_1 = __importDefault(require("../../config"));
const mock_data_1 = require("../../mock-data");
test('static graph test', async () => {
    appeng_db_1.AppengDBConfig.configure(config_1.default.get('db'), 'sqlite3');
    const serviceOrchestrator = appeng_db_1.AppengDBConfig.INSTANCE;
    const servicesMock = sinon_1.default.mock(serviceOrchestrator);
    servicesMock.expects('selectSingleRecordUsingQuery').never();
    const graph = noflo_1.default.graph.createGraph('InboundBusinessRule');
    graph.addNode('BusinessRule', 'ScriptExecutor');
    graph.addInitial(mock_data_1.BusinessRules[2].rule, 'BusinessRule', 'rule');
    graph.addInitial(serviceOrchestrator, 'BusinessRule', 'serviceorchestrator');
    graph.addInport('in', 'BusinessRule', 'in');
    graph.addOutport('out', 'BusinessRule', 'out');
    const baseDir = path.resolve(__dirname, '../../');
    const loader = new noflo_1.default.ComponentLoader(baseDir);
    const options = {
        loader,
    };
    const wrappedGraph = noflo_1.default.asCallback(graph, options);
    const promisedGraph = util_1.default.promisify(wrappedGraph);
    const result = await promisedGraph(mock_data_1.FormData);
    sinon_1.default.assert.match(result.TARGETS, 264.00);
    servicesMock.restore();
});
//# sourceMappingURL=graph.static.test.js.map