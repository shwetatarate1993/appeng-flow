"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const appeng_db_1 = require("appeng-db");
const sinon_1 = __importDefault(require("sinon"));
const config_1 = __importDefault(require("../../config"));
const executors_1 = require("../../executors");
const mock_data_1 = require("../../mock-data");
test('Testing of data preprocessor executor', async () => {
    appeng_db_1.AppengDBConfig.configure(config_1.default.get('db'), 'sqlite3');
    const serviceOrchestrator = appeng_db_1.AppengDBConfig.INSTANCE;
    const servicesMock = sinon_1.default.mock(serviceOrchestrator);
    servicesMock.expects('selectSingleRecordUsingQuery').atLeast(1).returns({});
    const result = await executors_1.dataPreprocessorsExecutor(mock_data_1.DataPreprocessors, mock_data_1.PreProcessorFormData, serviceOrchestrator);
    sinon_1.default.assert.match(result[0].FAMILIES_WITHOUT_TOILETS, 14);
    servicesMock.restore();
});
//# sourceMappingURL=data.preprocessor.executor.test.js.map