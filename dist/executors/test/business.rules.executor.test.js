"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const appeng_db_1 = require("appeng-db");
const sinon_1 = __importDefault(require("sinon"));
const config_1 = __importDefault(require("../../config"));
const executors_1 = require("../../executors");
const mock_data_1 = require("../../mock-data");
test('Testing of business rule executor', async () => {
    appeng_db_1.AppengDBConfig.configure(config_1.default.get('db'), 'sqlite3');
    const serviceOrchestrator = appeng_db_1.AppengDBConfig.INSTANCE;
    const servicesMock = sinon_1.default.mock(serviceOrchestrator);
    servicesMock.expects('selectSingleRecordUsingQuery').atLeast(1).returns({});
    const result = await executors_1.businessRulesExecutor(mock_data_1.BusinessRules, mock_data_1.FormData, serviceOrchestrator);
    sinon_1.default.assert.match(result.TARGETS, 196.00);
    sinon_1.default.assert.match(result.NEW_UUID !== '', true);
    servicesMock.restore();
});
//# sourceMappingURL=business.rules.executor.test.js.map