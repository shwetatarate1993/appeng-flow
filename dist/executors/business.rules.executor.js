"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require(".");
const graph_builders_1 = require("../graph-builders");
exports.businessRulesExecutor = async (businessRules, formData, serviceOrchestrator) => {
    const graph = graph_builders_1.businessRulesGraphBuilder(businessRules, serviceOrchestrator);
    return await _1.graphExecutor(graph, formData);
};
//# sourceMappingURL=business.rules.executor.js.map