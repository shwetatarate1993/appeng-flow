import { ServiceOrchestrator } from 'appeng-db';
import { DataPreProcessor } from '../models';
export declare const dataPreprocessorsExecutor: (dataPreprocessors: DataPreProcessor[], formData: any, serviceOrchestrator: ServiceOrchestrator) => Promise<any>;
//# sourceMappingURL=data.preprocessors.executor.d.ts.map