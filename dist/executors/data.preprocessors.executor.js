"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require(".");
const graph_builders_1 = require("../graph-builders");
exports.dataPreprocessorsExecutor = async (dataPreprocessors, formData, serviceOrchestrator) => {
    const graph = graph_builders_1.preprocessorsGraphBuilder(dataPreprocessors, serviceOrchestrator);
    return await _1.graphExecutor(graph, formData);
};
//# sourceMappingURL=data.preprocessors.executor.js.map