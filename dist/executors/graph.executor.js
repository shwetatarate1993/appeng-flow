"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const noflo_1 = __importDefault(require("noflo"));
const path = __importStar(require("path"));
const util_1 = __importDefault(require("util"));
const ae_component_loader_1 = __importDefault(require("../aeflo/ae.component.loader"));
exports.graphExecutor = async (graph, formData) => {
    const baseDir = path.resolve(__dirname, '../');
    const loader = new noflo_1.default.ComponentLoader(baseDir);
    loader.registerLoader(ae_component_loader_1.default, (err) => {
        console.log('Error in external loader:' + err);
    });
    const options = {
        loader,
    };
    const wrappedGraph = noflo_1.default.asCallback(graph, options);
    const promisedGraph = util_1.default.promisify(wrappedGraph);
    return await promisedGraph(formData);
};
//# sourceMappingURL=graph.executor.js.map