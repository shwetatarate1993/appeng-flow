import { ServiceOrchestrator } from 'appeng-db';
import { NodeBusinessRule } from '../models';
export declare const businessRulesExecutor: (businessRules: NodeBusinessRule[], formData: any, serviceOrchestrator: ServiceOrchestrator) => Promise<any>;
//# sourceMappingURL=business.rules.executor.d.ts.map