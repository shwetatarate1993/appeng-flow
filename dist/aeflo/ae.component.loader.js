"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ScriptExecutor_1 = require("../components/ScriptExecutor");
const ScriptExecutorConst = 'ScriptExecutor';
const aeComponentLoader = (nofloLoader, callback) => {
    nofloLoader.components = nofloLoader.components ? nofloLoader.components : {};
    nofloLoader.components[ScriptExecutorConst] = ScriptExecutor_1.getComponent;
    nofloLoader.ready = true;
};
exports.default = aeComponentLoader;
//# sourceMappingURL=ae.component.loader.js.map