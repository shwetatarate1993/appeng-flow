"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const appeng_db_1 = require("appeng-db");
const sinon_1 = __importDefault(require("sinon"));
const __1 = require("../");
const config_1 = __importDefault(require("../../config"));
const mock_data_1 = require("../../mock-data");
test('Testing of graph builder for list of business rules', async () => {
    appeng_db_1.AppengDBConfig.configure(config_1.default.get('db'), 'sqlite3');
    const serviceOrchestrator = appeng_db_1.AppengDBConfig.INSTANCE;
    const servicesMock = sinon_1.default.mock(serviceOrchestrator);
    servicesMock.expects('selectSingleRecordUsingQuery').never();
    const graph = await __1.businessRulesGraphBuilder(mock_data_1.BusinessRules, serviceOrchestrator);
    sinon_1.default.assert.match(graph.nodes.length, mock_data_1.BusinessRules.length);
    sinon_1.default.assert.match(graph.edges.length, mock_data_1.BusinessRules.length - 1);
    sinon_1.default.assert.match(graph.name, 'BusinessRulesGraph');
    servicesMock.restore();
});
//# sourceMappingURL=business.rule.graph.builder.test.js.map