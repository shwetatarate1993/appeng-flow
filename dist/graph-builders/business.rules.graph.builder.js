"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const noflo_1 = __importDefault(require("noflo"));
const sort_objects_1 = require("../utils/sort.objects");
exports.businessRulesGraphBuilder = (businessRules, serviceOrchestrator) => {
    sort_objects_1.sortObjects(businessRules);
    const graph = noflo_1.default.graph.createGraph('BusinessRulesGraph');
    let previousNodeName = '';
    for (const nodeRule of businessRules) {
        graph.addNode(nodeRule.configObjectId, 'ScriptExecutor');
        graph.addInitial(nodeRule.rule, nodeRule.configObjectId, 'rule');
        graph.addInitial(serviceOrchestrator, nodeRule.configObjectId, 'serviceorchestrator');
        if (previousNodeName) {
            graph.addEdge(previousNodeName, 'out', nodeRule.configObjectId, 'in');
        }
        else {
            graph.addInport('in', nodeRule.configObjectId, 'in');
        }
        previousNodeName = nodeRule.configObjectId;
    }
    const lastRuleIndex = businessRules.length - 1;
    graph.addOutport('out', businessRules[lastRuleIndex].configObjectId, 'out');
    return graph;
};
//# sourceMappingURL=business.rules.graph.builder.js.map