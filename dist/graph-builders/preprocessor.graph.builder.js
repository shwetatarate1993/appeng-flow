"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const noflo_1 = __importDefault(require("noflo"));
const sort_objects_1 = require("../utils/sort.objects");
exports.preprocessorsGraphBuilder = (dataPreprocessors, serviceOrchestrator) => {
    sort_objects_1.sortObjects(dataPreprocessors);
    const graph = noflo_1.default.graph.createGraph('DataPreprocessorGraph');
    let previousNodeName = '';
    for (const preprocessor of dataPreprocessors) {
        graph.addNode(preprocessor.configObjectId, 'ScriptExecutor');
        graph.addInitial(preprocessor.jsCode, preprocessor.configObjectId, 'rule');
        graph.addInitial(serviceOrchestrator, preprocessor.configObjectId, 'serviceorchestrator');
        if (previousNodeName) {
            graph.addEdge(previousNodeName, 'out', preprocessor.configObjectId, 'in');
        }
        else {
            graph.addInport('in', preprocessor.configObjectId, 'in');
        }
        previousNodeName = preprocessor.configObjectId;
    }
    const lastRuleIndex = dataPreprocessors.length - 1;
    graph.addOutport('out', dataPreprocessors[lastRuleIndex].configObjectId, 'out');
    return graph;
};
//# sourceMappingURL=preprocessor.graph.builder.js.map