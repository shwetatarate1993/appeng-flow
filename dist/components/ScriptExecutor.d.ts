import { ServiceOrchestrator } from 'appeng-db';
export declare const getComponent: () => any;
export declare const createFunction: (expression: string) => Promise<any>;
export declare const executeGenericExpression: (input: any, expression: string, serviceOrchestrator: ServiceOrchestrator) => Promise<any>;
//# sourceMappingURL=ScriptExecutor.d.ts.map