"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const noflo_1 = __importDefault(require("noflo"));
const uuid_1 = require("uuid");
const constant_1 = require("../constant");
/* tslint:disable-next-line:no-var-requires */
const amountToWord = require('written-number');
exports.getComponent = () => {
    const c = new noflo_1.default.Component();
    c.description = 'execution of business rule';
    c.inPorts.add('rule', {
        datatype: 'string',
        description: 'business rule config',
        control: true,
    });
    c.inPorts.add('serviceorchestrator', {
        datatype: 'object',
        description: 'serviceOrchestrator object',
    });
    c.inPorts.add('in', {
        datatype: 'object',
        description: 'FormData(input Data)',
    });
    c.outPorts.add('out', {
        datatype: 'object',
        description: 'processed formData(Output Data)',
    });
    return c.process(async (input, output) => {
        if (!input.has('in') || !input.has('serviceorchestrator')) {
            return;
        }
        if (input[constant_1.EXCEPTION_KEY]) {
            output.sendDone({
                out: input,
            });
        }
        const params = input.getData('in');
        const rule = input.getData('rule');
        const serviceOrchestrator = input.getData('serviceorchestrator');
        const outputParams = await executeRule(rule, params, serviceOrchestrator);
        output.sendDone({
            out: outputParams,
        });
    });
};
const executeRule = async (rule, input, serviceOrchestrator) => {
    const result = await exports.executeGenericExpression(input, rule, serviceOrchestrator);
    return result;
};
exports.createFunction = async (expression) => {
    
    expression = expression? expression.replace(/(\\n)/gm, ';'):'';
    expression = expression? expression.replace(/\\/g, ''):'';
    const strFunction = 'return async function(dependecies,input) {  var serviceOrchestrator = dependecies.serviceOrchestrator;\n'
        + ' var moment = dependecies.moment; \n'
        + ' var amountToWord = dependecies.amountToWord;\n'
        + ' var uuid = dependecies.uuid;\n'
        + expression + '\n' + 'return input;}';
    const dynamicFunction = new Function('dependecies', 'input', strFunction)();
    return dynamicFunction;
};
exports.executeGenericExpression = async (input, expression, serviceOrchestrator) => {
    const dependecies = {
        serviceOrchestrator,
        amountToWord,
        moment: moment_1.default,
        uuid: uuid_1.v4,
    };
    try {
        const exprFunction = await exports.createFunction(expression);
        return await exprFunction(dependecies, input);
    }
    catch (error) {
        console.log('script execution failed.....', error);
        input[constant_1.EXCEPTION_KEY] = error;
        return input;
    }
};
//# sourceMappingURL=ScriptExecutor.js.map