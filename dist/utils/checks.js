"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* istanbul ignore next */
function assertNever(msg, value) {
    throw new Error(msg + ': ' + JSON.stringify(value));
}
exports.assertNever = assertNever;
//# sourceMappingURL=checks.js.map