"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sortObjects = (objects) => {
    objects.sort((a, b) => {
        return a.order - b.order;
    });
};
//# sourceMappingURL=sort.objects.js.map