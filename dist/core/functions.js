"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../utils");
function evaluate(expr) {
    switch (expr.type) {
        case 'literal': {
            return expr.value;
        }
        case 'binary': {
            switch (expr.operator) {
                case '+':
                    return evaluate(expr.left) + evaluate(expr.right);
                case '-':
                    return evaluate(expr.left) - evaluate(expr.right);
                case '*':
                    return evaluate(expr.left) * evaluate(expr.right);
                case '/':
                    return evaluate(expr.left) / evaluate(expr.right);
                /* istanbul ignore next */
                default: {
                    return utils_1.assertNever('Unexpected binary operator', expr.operator);
                }
            }
        }
        /* istanbul ignore next */
        default: {
            return utils_1.assertNever('Unexpected expression type', expr);
        }
    }
}
exports.evaluate = evaluate;
//# sourceMappingURL=functions.js.map