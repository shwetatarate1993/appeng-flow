export interface Literal {
    type: 'literal';
    value: number;
}
export declare type BinaryOperators = '+' | '-' | '*' | '/';
export interface BinaryOperation {
    type: 'binary';
    operator: BinaryOperators;
    left: Expression;
    right: Expression;
}
export declare type Expression = Literal | BinaryOperation;
//# sourceMappingURL=types.d.ts.map