"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: 'd4588e5b-153c-4dcb-a9d1-f41819587ca2',
        executionType: 'Javascript',
        jsCode: 'var fam = (input[0][\"TOTAL_FAMILIES\"] ?parseInt(input[0][\"TOTAL_FAMILIES\"]):0)\n'
            + '-(input[0][\"FAMILIES_HAVING_TOILETS\"] ?parseInt(input[0][\"FAMILIES_HAVING_TOILETS\"]):0);\n\n'
            + 'input[0][\"FAMILIES_WITHOUT_TOILETS\"] = parseInt(fam);',
        order: 5,
        name: 'Calculate Total Families Without Toilet',
    },
];
//# sourceMappingURL=DataPreprocessors.js.map