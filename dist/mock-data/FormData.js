"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    INDIRA_AWAS_YOJANA_UUID: 1111,
    GRAMPANCHAYAT_NAME: null,
    SECOND_INSTALLMENT: 88,
    FIRST_INSTALLMENT: 88,
    FY_YEAR: null,
    TARGETS: null,
    INDIRA_AWAS_YOJANA_ID: null,
    RANK: null,
    FINAL_INSTALLMENT: 88,
    CREATED_ON: '2019-08-31T18:30:00.000Z',
    NEW_UUID: null,
};
//# sourceMappingURL=FormData.js.map