declare const _default: {
    0: {
        child: {};
        APP_LOGGED_IN_USER_GP: number;
        APP_LOGGED_IN_ROLE_ID: number;
        APP_LOGGED_IN_YEAR: number;
        APP_LOGGED_IN_USER_TALUKA: number;
        APP_LOGGED_IN_USER_DISTRICT: number;
        APP_LOGGED_IN_USER_STATE: number;
        APP_LOGGED_IN_USER_ID: number;
        CLEAN_INDIA_MISSION_ID: any;
        FAMILIES_WITHOUT_TOILETS: any;
        GRAMPANCHAYAT: number;
        CLEAN_INDIA_MISSION_UUID: any;
        TOTAL_FAMILIES: string;
        FAMILIES_HAVING_TOILETS: string;
        TALUKA: string;
        TOILET_BUILT_DURING_THE_WEEK: string;
    };
};
export default _default;
//# sourceMappingURL=PreProcessorFormData.d.ts.map