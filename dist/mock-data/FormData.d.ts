declare const _default: {
    INDIRA_AWAS_YOJANA_UUID: number;
    GRAMPANCHAYAT_NAME: any;
    SECOND_INSTALLMENT: number;
    FIRST_INSTALLMENT: number;
    FY_YEAR: any;
    TARGETS: any;
    INDIRA_AWAS_YOJANA_ID: any;
    RANK: any;
    FINAL_INSTALLMENT: number;
    CREATED_ON: string;
    NEW_UUID: any;
};
export default _default;
//# sourceMappingURL=FormData.d.ts.map