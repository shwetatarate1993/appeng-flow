declare const _default: {
    configObjectId: string;
    executionType: string;
    jsCode: string;
    order: number;
    name: string;
}[];
export default _default;
//# sourceMappingURL=DataPreprocessors.d.ts.map