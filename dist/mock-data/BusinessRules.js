"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        configObjectId: '6ef43bb3-e435-44fd-a762-d8e3ac6a9532',
        executionType: 'Custom',
        rule: 'input["TARGETS"] =10;input["NEW_UUID"] =uuid()',
        order: 5,
        name: 'CalculateTotalSubmissionForTaxCollectionDetails',
    },
    {
        configObjectId: '9f315754-7ed0-45fc-8c04-3f87b8f2ea6',
        executionType: 'Javascript',
        rule: 'input["FIRST_INSTALLMENT"] =20',
        order: 10,
        name: 'Payment Distribution',
    },
    {
        configObjectId: '9f315754-7ed0-45fc-8c04-3f87b8f2eefa',
        executionType: 'Javascript',
        rule: 'var targets = (parseFloat((!input["FIRST_INSTALLMENT"]) ?0:'
            + 'input["FIRST_INSTALLMENT"])\n+ parseFloat((!input["SECOND_INSTALLMENT"]) ?0:'
            + 'input["SECOND_INSTALLMENT"])\n+ parseFloat((!input["FINAL_INSTALLMENT"]) ?0:'
            + 'input["FINAL_INSTALLMENT"]));\n\n input["TARGETS"] = parseFloat(targets).toFixed(2);\n',
        order: 20,
        name: 'Payment Distribution',
    },
    {
        configObjectId: '9f315754-7ed0-45fc-8c04-3f87b8f2e',
        executionType: 'Javascript',
        rule: 'await serviceOrchestrator.selectSingleRecordUsingQuery(\'mock\',\'\',{});',
        order: 15,
        name: 'Payment Distribution',
    },
];
//# sourceMappingURL=BusinessRules.js.map