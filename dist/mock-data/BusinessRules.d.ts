declare const _default: {
    configObjectId: string;
    executionType: string;
    rule: string;
    order: number;
    name: string;
}[];
export default _default;
//# sourceMappingURL=BusinessRules.d.ts.map